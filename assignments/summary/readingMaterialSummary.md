### Module1_summary:-      
**IOT:- Internet of things**  
**The Internet of Things (IoT) is a system of interrelated computing devices, mechanical and digital machines, objects, animals or people that are provided with unique identifiers and the ability to transfer data over a network without requiring human-to-human or human-to-computer interaction.

![](https://www.thesandreckoner.co.uk/wp-content/uploads/2015/01/IoT_Cisco.png)

* IOT is nothing but things connected to the internet for better controlling and accessing.
* Industry is evolving day by day from Industry 1 to industry 4.
* From mechanical to the internet, from steam power to cyber physical systems is the present scenario.

![](https://coconetinc.files.wordpress.com/2017/09/industry-4-0-v01.png)

### **INDUSTRY 3.0**  
 In Industry 3.0, we automate processes using logic processors and information technology. These processes often operate largely without human interference, but there is still a human aspect behind it.     
 ![](https://www.precicon.com.sg/wp-content/uploads/2020/03/Third-Industrial-Revolution.png)

**INDUSTRY 3.0 ARCHITECTURE**
* sensors usually detect physical parameters and send data to PLC's using field bus. PLC's send data to SCADA and ERP for storing of the data. We can use this data to make statistics and make use of this data to easily understand the working of the machine or the thing.   

**INDUSTRY 3.0 COMMUNICATION PROTOCOLS**
The data transfer is mainly done using these communication protocols.
* Modbus
* ethercat
* profit net
* CAN open are basically used for transfer of data. These protocols are generally known as field bus.

Due to the change in industry trend Industry 3.0 to be changed or modified to industry 4.0.

**INDUSTRY 4.0 ARCHITECTURE**    
   Industry 4.0 has some more stages compared to industry 3.0. Edge stage and cloud stage are new to Industry 4.0 with which we can Remote control configuration of devices, provide Predctive Maintenance, Do Remote Web SCADA, Real time event stream processing, Analytics with predictive models, Automated Device
provisioning, Auto discovery, also get Real time alerts & alarms

![](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTCvr6yV4YKy9mXcejKwbR1jV8CsMrk6g8fvg&usqp=CAU)

**INDUSTRY COMMUNICATION PROTOCOLS**
* MQTT
* AMQP
* OPCUA
* CoAP
* WEBSOCKETS
* HTTP
* RESTFUL API are some of the protocols which can be used in Industry 4.0.

Industry 3.0 can be changed to Industry 4.0 by simply changing the protocols without changing the entire Industry. 

![](https://qph.fs.quoracdn.net/main-qimg-25782847f73e3000e5dd12704168ad18.webp)

**IoT TSDB tools**
Time series databases allow businesses to store time-stamped data. A company may adopt a time series database if they need to monitor data in real time or if they are running applications that continuously produce data. Store your data in Time series databases
* Prometheus 
* influx DB are famous tools.

**IOT Dashboards**
An Internet of Things (IoT) dashboard is a data visualization tool that transforms, displays, and organizes a collection of data captured and transmitted by network connected devices. These are used to make statistics using the data.
* Grafana
* ThingsBoard.

**IOT platforms**  
An IoT platform enables IoT device and endpoint management, connectivity and network management, data management, processing and analysis, application development, security, access control, monitoring, event processing and interfacing/integration. These are used to send data to the cloud and retrieve data from the cloud.
* AWS IOT
* GOOGLE IOT
* AZURE IOT
* THINGSBOARD.

**Get Alerts**
Get Alerts based on your data using these platforms
* Zaiper
* Twilio.

